import 'package:flutter/material.dart';



class HToD extends StatefulWidget {
  @override
  _HToDState createState() => _HToDState();
}

class _HToDState extends State<HToD> {
  TextEditingController hController = TextEditingController();

  final int constante = 24;

  String _hToD = "Informe quantas horas você quer converter";

  void _resetFields() {
    hController.text = "";
    setState(() {
      _hToD = "Informe quantas horas você quer converter";
    });
  }

  void _htToDia() {
    setState(() {
      double hora = double.parse(hController.text);
      double dia = hora / constante;
      return _hToD = dia.toStringAsFixed(2);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Horas para Dias",
          style: TextStyle(fontSize: 16),
        ),
        centerTitle: true,
        backgroundColor: Colors.green,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              _resetFields();
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.fromLTRB(15.0, 50, 15.0, 0.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    //Decoração do input
                    decoration: InputDecoration(
                      // Nome do input
                      labelText: "Horas",
                      //Estilo do input
                      labelStyle: TextStyle(color: Colors.black),
                    ),
                    //Colocando o texto do input no centro
                    textAlign: TextAlign.center,
                    //Mexendo no style do input
                    style: TextStyle(color: Colors.black, fontSize: 16.0),
                    //Declarando o controlador do input
                    controller: hController,
                  )),
              Padding(
                padding: EdgeInsets.only(left: 15.0, top: 20, right: 15.0),
                child: Container(
                  height: 50.0,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.green)),
                    onPressed: () {
                      _htToDia();
                    },
                    color: Colors.green,
                    child: Text(
                      "Converter",
                      style: TextStyle(fontSize: 16,color: Colors.white),
                    ),
                  ),
                ),
              ),
              Center(
                child: Container(
                    margin: EdgeInsets.all(15.0),
                    padding: EdgeInsets.all(50.0),
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.green,
                          width: 10.0,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(18.0)),
                        color: Colors.white),
                    child: Center(
                      child: Text(
                        " $_hToD",
                        style: TextStyle(fontSize: 18),
                        textAlign: TextAlign.center,
                      ),
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}