import 'package:flutter/material.dart';
import 'package:medidas/kmM.dart';
import 'package:medidas/horasD.dart';
import 'package:medidas/miH.dart';
import 'package:medidas/meKm.dart';
import 'package:medidas/diaH.dart';
import 'package:medidas/miKm.dart';





void main() {
  runApp(MaterialApp(
    color: Colors.black,
    home: Home(),
    theme: ThemeData(hintColor: Colors.black, primaryColor: Colors.red),
  ));
}



class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(centerTitle: true,title: Text("CONVERSOR",style:TextStyle(fontSize: 25),),backgroundColor: Color.fromRGBO(52, 152, 219,1.0),),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Text("Menu",style: TextStyle(fontSize: 20,color: Colors.white)),
              decoration: BoxDecoration(color: Color.fromRGBO(192, 57, 43,1.0),),
            ),
            Container(
              color: Colors.red,
              child: ListTile(title: Text("Quilômetros Para Milhas",style: TextStyle(color: Colors.white),),
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => KmToM()),
                );
              } ,
              ),
            ),
            SizedBox(
              height: 5.5,
            ),
            Container(
              color: Color.fromRGBO(68, 189, 50,1.0),
              child: ListTile(title: Text("Horas Para Dias",style: TextStyle(color: Colors.white)),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HToD()),
                  );
                } ,
              ),
            ),
            SizedBox(
              height: 5.5,
            ),
            Container(
              color: Color.fromRGBO(53, 59, 72,1.0),
              child: ListTile(title: Text("Milhas Em Quilômetros",style: TextStyle(color: Colors.white)),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MToKm()),
                  );
                } ,
              ),
            ),
            SizedBox(
              height: 5.5,
            ),
            Container(
              color: Colors.deepPurple,
              child: ListTile(title: Text("Dias Em Horas",style: TextStyle(color: Colors.white)),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => DToH()),
                  );
                } ,
              ),
            ),
            SizedBox(
              height: 5.5,
            ),
            Container(
              color: Colors.blueAccent,
              child: ListTile(title: Text("Metros Em Quilômetros",style: TextStyle(color: Colors.white)),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MeToKm()),
                  );
                } ,
              ),
            ),
            SizedBox(
              height: 5.5,
            ),
            Container(
              color: Colors.orange,
              child: ListTile(title: Text("Minutos Em Horas",style: TextStyle(color: Colors.white)),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MToH()),
                  );
                } ,
              ),
            ),
          ],
        )
      ),
        backgroundColor: Color.fromRGBO(52, 152, 219,1.0) ,
      ),
    );
  }
}
