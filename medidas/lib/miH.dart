
import 'package:flutter/material.dart';


class MToH extends StatefulWidget {
  @override
  _MToHState createState() => _MToHState();
}

class _MToHState extends State<MToH> {
  TextEditingController mController = TextEditingController();

  final int constante = 60;

  String _horas = "Informe quantos minutos você quer converter";

  void _resetFields() {
    mController.text = "";
    setState(() {
      _horas = "Informe quantos minutos você quer converter";
    });
  }

  void _mtoH() {
    setState(() {
      double min = double.parse(mController.text);
      double hora = min / constante;
      return _horas = hora.toStringAsFixed(2);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Minutos para Horas",
          style: TextStyle(fontSize: 16),
        ),
        centerTitle: true,
        backgroundColor: Colors.deepOrange,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              _resetFields();
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.fromLTRB(15.0, 50, 15.0, 0.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    //Decoração do input
                    decoration: InputDecoration(
                      // Nome do input
                      labelText: "Minutos",
                      //Estilo do input
                      labelStyle: TextStyle(color: Colors.black),
                    ),
                    //Colocando o texto do input no centro
                    textAlign: TextAlign.center,
                    //Mexendo no style do input
                    style: TextStyle(color: Colors.black, fontSize: 16.0),
                    //Declarando o controlador do input
                    controller: mController,
                  )),
              Padding(
                padding: EdgeInsets.only(left: 15.0, top: 20, right: 15.0),
                child: Container(
                  height: 50.0,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.deepOrangeAccent)),
                    onPressed: () {
                      _mtoH();
                    },
                    color: Colors.deepOrangeAccent,
                    child: Text(
                      "Converter",
                      style: TextStyle(fontSize: 16,color: Colors.white),
                    ),
                  ),
                ),
              ),
              Center(
                child: Container(
                    margin: EdgeInsets.all(15.0),
                    padding: EdgeInsets.all(50.0),
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.deepOrangeAccent,
                          width: 10.0,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(18.0)),
                        color: Colors.white),
                    child: Center(
                      child: Text(
                        " $_horas",
                        style: TextStyle(fontSize: 18),
                        textAlign: TextAlign.center,
                      ),
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
