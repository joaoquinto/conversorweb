import 'package:flutter/material.dart';



//Metros para Km//

class MeToKm extends StatefulWidget {
  @override
  _MeToKmState createState() => _MeToKmState();
}

class _MeToKmState extends State<MeToKm> {
  TextEditingController mController = TextEditingController();

  final int metros = 1000;

  String _meTokm = "Informe a metragem para converter para quilômetros";

  void _resetFields() {
    mController.text = "";
    setState(() {
      _meTokm = "Informe a metragem para converter para quilômetros";
    });
  }

  void _meeToKm() {
    setState(() {
      double me = double.parse(mController.text);
      double km = me / metros;
      return _meTokm = km.toString();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
            "Metros Para Km",
            style: TextStyle(fontSize: 16),
          ),
          centerTitle: true,
          backgroundColor: Colors.blue,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                _resetFields();
              },
            ),
          ]),
      body: SingleChildScrollView(
        child: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.fromLTRB(15.0, 50, 15.0, 0.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    //Decoração do input
                    decoration: InputDecoration(
                      // Nome do input
                      labelText: "Metros",
                      //Estilo do input
                      labelStyle: TextStyle(color: Colors.black),
                    ),
                    //Colocando o texto do input no centro
                    textAlign: TextAlign.center,
                    //Mexendo no style do input
                    style: TextStyle(color: Colors.black, fontSize: 16.0),
                    //Declarando o controlador do input
                    controller: mController,
                  )),
              Padding(
                padding: EdgeInsets.only(left: 15.0, top: 20, right: 15.0),
                child: Container(
                  height: 50.0,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.blue)),
                    onPressed: () {
                      _meeToKm();
                    },
                    color: Colors.blue,
                    child: Text(
                      "Converter",
                      style: TextStyle(fontSize: 16,color: Colors.white),
                    ),
                  ),
                ),
              ),
              Center(
                child: Container(
                    margin: EdgeInsets.all(15.0),
                    padding: EdgeInsets.all(50.0),
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.blue,
                          width: 10.0,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(18.0)),
                        color: Colors.white),
                    child: Center(
                      child: Text(
                        "$_meTokm ",
                        style: TextStyle(fontSize: 18),
                        textAlign: TextAlign.center,
                      ),
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
