import 'package:flutter/material.dart';



class MToKm extends StatefulWidget {
  @override
  _MToKmState createState() => _MToKmState();
}

class _MToKmState extends State<MToKm> {
  TextEditingController miController = TextEditingController();

  final double milha = 1.60934;

  String _mTokm = "Informe as suas milhas para converter para quilômetros";

  void _resetFields() {
    miController.text = "";
    setState(() {
      _mTokm = "Informe as suas milhas para converter para quilômetros";
    });
  }

  void _miToKm() {
    setState(() {
      double mi = double.parse(miController.text);
      double km = mi * milha;
      return _mTokm = km.toStringAsFixed(5);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
            "Milhas Para Quilômetros",
            style: TextStyle(fontSize: 16, color: Colors.white),
          ),
          centerTitle: true,
          backgroundColor: Color.fromRGBO(53, 59, 72,1.0),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.refresh,
                color: Colors.black,
              ),
              onPressed: () {
                _resetFields();
              },
            ),
          ]),
      body: SingleChildScrollView(
        child: Form(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.fromLTRB(15.0, 50, 15.0, 0.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    //Decoração do input
                    decoration: InputDecoration(
                      // Nome do input
                      labelText: "Milhas",
                      //Estilo do input
                      labelStyle: TextStyle(color: Colors.black),
                    ),
                    //Colocando o texto do input no centro
                    textAlign: TextAlign.center,
                    //Mexendo no style do input
                    style: TextStyle(color: Colors.black, fontSize: 16.0),
                    //Declarando o controlador do input
                    controller: miController,
                  )),
              Padding(
                padding: EdgeInsets.only(left: 15.0, top: 20, right: 15.0),
                child: Container(
                  height: 50.0,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                        side: BorderSide(color: Color.fromRGBO(53, 59, 72,1.0))),
                    onPressed: () {
                      _miToKm();
                    },
                    color: Color.fromRGBO(53, 59, 72,1.0),
                    child: Text(
                      "Converter",
                      style: TextStyle(fontSize: 16,color: Colors.white),
                    ),
                  ),
                ),
              ),
              Center(
                child: Container(
                    margin: EdgeInsets.all(15.0),
                    padding: EdgeInsets.all(50.0),
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Color.fromRGBO(53, 59, 72,1.0),
                          width: 10.0,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(18.0)),
                        color: Colors.white),
                    child: Center(
                      child: Text(
                        "$_mTokm ",
                        style: TextStyle(fontSize: 18),
                        textAlign: TextAlign.center,
                      ),
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }
}